from django.db import models
from django.contrib.auth.models import AbstractBaseUser, BaseUserManager

# Create your models here.
class AccountManager(BaseUserManager):
    def create_user(self,email,mobile_no,first_name,last_name,password=None):
        if not email:
            raise ValueError("Users must have an email address")
        if not mobile_no:
            raise ValueError("Users must have an mobile_no")
        if not first_name:
            raise ValueError("Users must have an firstname")
        if not last_name:
            raise ValueError("Users must have an lastname")
        
        '''
        This validation doesn't need to be here, It will only raise an Exception. 
        So make sure to handle it in Serializer or in Model fiels declaration or 
        Model's clean method (for admin panel)
        '''
        

        user = self.model(
            email=self.normalize_email(email),
            mobile_no= mobile_no,
            first_name=first_name,
            last_name=last_name
        )
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self,email,mobile_no,password,first_name,last_name):
        user = self.create_user(
            email=self.normalize_email(email),
            mobile_no= mobile_no,
            password=password,
            first_name=first_name,
            last_name=last_name
        )
        user.is_admin = True
        user.is_staff = True
        user.is_superuser = True
        user.save(using=self._db)
        return user


class Account(AbstractBaseUser):
    email = models.EmailField(verbose_name="email",max_length=60,unique=True)
    mobile_no = models.CharField(verbose_name="mobile",max_length=60,unique=True)
    date_joined = models.DateTimeField(verbose_name="date joined",auto_now_add=True)
    last_login = models.DateTimeField(verbose_name="last login",auto_now=True)
    is_active = models.BooleanField(default=True)
    is_admin = models.BooleanField(default=False)
    is_staff = models.BooleanField(default=False)
    is_superuser = models.BooleanField(default=False)
    first_name = models.CharField(verbose_name="firstname",max_length=30)
    last_name = models.CharField(verbose_name="lastname",max_length=30)
    is_verified = models.BooleanField(default=False)

    # Field to identify the user,
    USERNAME_FIELD = "email"
    # Required fields
    REQUIRED_FIELDS = ['mobile_no','first_name','last_name']

    # Tell Dja go to user the custom defined user model.
    objects = AccountManager()

    def __str__(self):
        return self.email

    def has_perm(self, perm, obj=None):
        return True

    def has_module_perms(self, app_label):
        return True
from rest_framework import serializers
from .models import Account
from rest_framework.exceptions import ValidationError

class UserCreateSerializer(serializers.ModelSerializer):
    email = serializers.EmailField()
    mobile_no = serializers.CharField()
    password1 = serializers.CharField(write_only=True)
    password2 = serializers.CharField(write_only=True)
    
    class Meta:
        model = Account
        fields = ['email','mobile_no','first_name',"last_name","password1","password2"]
    
    def check_if_exists(self,email,mobile_no):
        email_status = Account.objects.filter(email=email).exists()
        mobile_status = Account.objects.filter(mobile_no=mobile_no).exists()
        if email_status == True or mobile_status == True:
            return True
        return False


    def create(self, validated_data):
        email = validated_data['email']
        mobile_no = validated_data['mobile_no']
        first_name = validated_data['first_name']
        last_name = validated_data['last_name']
        if validated_data['password1'] == validated_data['password2']:
            password = validated_data['password1']
        else:
            raise ValidationError(
                {'Email': ["Both Passwords dont match"]})
        # Check if user already exist
        if self.check_if_exists(email,mobile_no) == True:
            raise ValidationError(
                {'Email': ["User with this details already exists"]}
                )

        print("Email,mb is :- ",email,mobile_no)
        return Account.objects.create_user(email=email,mobile_no=mobile_no,first_name=first_name,last_name=last_name,password=password)
