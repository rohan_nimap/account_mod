from django.shortcuts import render
from rest_framework.generics import CreateAPIView
from .serializers import UserCreateSerializer
from .models import Account
# Create your views here.

class RegisterUser(CreateAPIView):
    serializer_class = UserCreateSerializer
    queryset = Account.objects.all()
