from django.db import models

# Create your models here.
class OTP(models.Model):
    email = models.EmailField(max_length=255,null=False)
    otp = models.CharField(max_length=6,null=False)
    
    # Our model name is OTP so generated_at or created at would be fine
    OTPGenerated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.email