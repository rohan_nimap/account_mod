from django.shortcuts import render
from .models import OTP
from accounts.models import Account
from rest_framework.views import APIView
from .serializers import OTPSerializer
# Create your views here.
from rest_framework.response import Response
from django.core.exceptions import ObjectDoesNotExist
from rest_framework.exceptions import AuthenticationFailed
from django.utils import timezone
from datetime import datetime, timedelta

class VerifyUser(APIView):
    
    def post(self,request):
        email = request.data.get('email')
        otp = request.data.get('otp')
        req_time = timezone.now()
        # Check if user exists and is already verified
        try:
            TargetUser = Account.objects.get(email__iexact=email)
            if TargetUser.is_verified == False:
                # Check for time difference >= 15 min
                try:
                    verified_email = OTP.objects.get(email__iexact=email,otp=otp,OTPGenerated_at__gte=(req_time-timedelta(15)))
                    TargetUser.is_verified = True
                    TargetUser.save()
                    return Response({'User Verified'})
                except ObjectDoesNotExist:
                    raise AuthenticationFailed('Incorrect Email/OTP', 'incorrect_otp')        
            else:
                return Response({'User Already Verified'})
        except ObjectDoesNotExist:
            raise AuthenticationFailed('User Doesnot Exist', 'No User')
