from django.contrib import admin
from django.urls import path
from .views import VerifyUser

urlpatterns = [
    path('verify/', VerifyUser.as_view()),
]