from rest_framework import serializers
from .models import OTP


class OTPSerializer(serializers.ModelSerializer):
    email = serializers.EmailField(max_length=255,read_only=True)
    otp = serializers.CharField(max_length=6,read_only=True)


    class Meta:
        model = OTP
        fields = '__all__'

